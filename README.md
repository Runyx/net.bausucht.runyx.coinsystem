# Commands
- /coins | Gibt das Spieler-Guthaben zurück
- /coins [playername] | Gibt das Guthaben des angegebenen Spielers zurück (Benötigt permission)
- /addcoins [playername|playerguid] [amount] | Fügt dem angegebenen Spieler (anhand Name oder GUID) Guthaben hinzu
- /removecoins [playername|playerguid] [amount] | Entfernt dem angegebenen Spieler (anhand Name oder GUID) Guthaben
- /setcoins [playername|playerguid] [amount] | Setzt dem angegebenen Spieler (anhand Name oder GUID) Guthaben
- /pay [playername> [amount] | Spieler zahlt an angegebenen Spieler Guthaben

# Events
- PlayerCoinsChangeEvent | Wird aufgerufen, wenn sich das Guthaben eines Spielers verändert
- PlayerPayPlayerEvent | Wird aufgerufen, wenn ein Spieler einem anderen Spieler Guthaben übergibt