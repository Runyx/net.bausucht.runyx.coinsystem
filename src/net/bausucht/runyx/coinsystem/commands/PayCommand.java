package net.bausucht.runyx.coinsystem.commands;

import net.bausucht.runyx.coinsystem.CoinSystem;
import net.bausucht.runyx.coinsystem.coins.manager.CoinsChangeResponse;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;

public class PayCommand implements CommandExecutor {

    @Override
    @Deprecated
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(player.hasPermission("coinsystem.pay")) {
                if(args.length == 2)
                {
                    String targetPlayerString = args[0];
                    if(player.getDisplayName().equals(targetPlayerString)) {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§4Du kannst dir selber keine Coins zahlen!");
                        return true;
                    }
                    int amount = Integer.valueOf(args[1]);

                    Player targetPlayer = Bukkit.getPlayer(targetPlayerString);

                    CoinsChangeResponse coinsChanged;

                    if(targetPlayer != null)
                    {
                        coinsChanged = CoinSystem.getCoinAPI().payCoins(player, targetPlayer, amount);
                    }
                    else
                    {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§4Der Spieler §e" + targetPlayerString + " §4ist nicht online!");
                        return true;
                    }

                    if(coinsChanged == CoinsChangeResponse.SUCCESS) {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§2Du hast dem Spieler §e" + targetPlayerString + " §e" + NumberFormat.getIntegerInstance().format(amount) + "§2 Coins gezahlt!");
                    }
                    else
                    {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§4Diesem Spieler konnten keine Coins gezahlt werden!");
                    }
                }
                else
                {
                    player.sendMessage(CoinSystem.getInstance().prefix + "/pay <playername> <amount>");
                }
                return true;
            }
            else
            {
                player.sendMessage("Unknown command. Type \"/help\" for help.");
                return false;
            }
        }
        return false;
    }
}
