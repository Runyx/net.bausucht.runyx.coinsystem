package net.bausucht.runyx.coinsystem.commands;

import net.bausucht.runyx.coinsystem.CoinSystem;
import net.bausucht.runyx.coinsystem.coins.manager.CoinsChangeResponse;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;

public class CoinsCommand implements CommandExecutor {

    @Override
    @Deprecated
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(args.length == 1)
            {
                if(player.hasPermission("coinsystem.coins")) {
                    String targetPlayerString = args[0];

                    Player targetPlayer = Bukkit.getPlayer(targetPlayerString);

                    int coins;

                    if (targetPlayer != null) {
                        coins = CoinSystem.getCoinAPI().getCoins(targetPlayer);
                    } else {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§4Der Spieler §e" + targetPlayerString + " §4ist nicht online!");
                        return true;
                    }

                    if (coins != -1) {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§2Der Spieler §e" + targetPlayerString + " §2hat §e" + NumberFormat.getIntegerInstance().format(coins) + " §2Coins!");
                    } else {
                        player.sendMessage(CoinSystem.getInstance().prefix + "§4Die Coins des Spielers konnten nicht angezeigt werden!");
                    }
                }
                else
                {
                    player.sendMessage(CoinSystem.getInstance().prefix + "/coins");
                }
            }
            else if(args.length == 0)
            {
                int coins = CoinSystem.getCoinAPI().getCoins(player);

                if (coins != -1) {
                    player.sendMessage(CoinSystem.getInstance().prefix + "§2Du hast §e" + NumberFormat.getIntegerInstance().format(coins) + " §2Coins!");
                } else {
                    player.sendMessage(CoinSystem.getInstance().prefix + "§4Deine Coins konnten nicht angezeigt werden!");
                }
            }
            else
            {
                player.sendMessage(CoinSystem.getInstance().prefix + "/coins");
            }
            return true;
        }
        return false;
    }
}
