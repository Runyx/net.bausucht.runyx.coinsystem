package net.bausucht.runyx.coinsystem.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerCoinsChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private boolean cancelled;

    private int oldAmount;
    private int newAmount;
    private Player player;

    public PlayerCoinsChangeEvent(Player player, int oldAmount, int newAmount) {
        this.oldAmount = oldAmount;
        this.newAmount = newAmount;
        this.player = player;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public int getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(int oldAmount) {
        this.oldAmount = oldAmount;
    }

    public int getNewAmount() {
        return newAmount;
    }

    public void setNewAmount(int newAmount) {
        this.newAmount = newAmount;
    }

    public Player getPlayer() {
        return player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
