package net.bausucht.runyx.coinsystem.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerPayPlayerEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private boolean cancelled;

    private int amount;
    private Player playerSender;
    private Player playerReceiver;

    public PlayerPayPlayerEvent(Player playerSender, Player playerReceiver, int amount) {
        this.amount = amount;
        this.playerSender = playerSender;
        this.playerReceiver = playerReceiver;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Player getPlayerSender() {
        return playerSender;
    }

    public Player getPlayerReceiver() {
        return playerReceiver;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
