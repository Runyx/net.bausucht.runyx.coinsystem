package net.bausucht.runyx.coinsystem;

import net.bausucht.runyx.coinsystem.coins.manager.CacheManager;
import net.bausucht.runyx.coinsystem.coins.CoinAPI;
import net.bausucht.runyx.coinsystem.coins.manager.MySQL;
import net.bausucht.runyx.coinsystem.coins.manager.MySqlManager;
import net.bausucht.runyx.coinsystem.commands.*;
import net.bausucht.runyx.coinsystem.listeners.SpecialCoinListeners;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class CoinSystem extends JavaPlugin {

    private static CoinSystem coinSystem;
    private static CoinAPI coinAPI;
    private static CacheManager cacheManager;

    private static MySQL mySql;

    public String prefix = "§6[Coins] §r";

    @Override
    public void onEnable() {
        coinSystem = this;

        coinAPI = new CoinAPI();
        cacheManager = new CacheManager();

        mySql = new MySQL("185.61.138.49", 3306, "bausucht", "bausucht", "dC3r&pdi@7LwG?4PmyU!E93o");

        Bukkit.getPluginManager().registerEvents(new SpecialCoinListeners(), this);

        registerCommandHandlers();

        registerCoinUpdater();
    }

    @Override
    public void onDisable() {
        MySqlManager.updateCoins();
    }

    private void registerCommandHandlers() {
        this.getCommand("coins").setExecutor(new CoinsCommand());
        this.getCommand("addcoins").setExecutor(new AddCoinsCommand());
        this.getCommand("removecoins").setExecutor(new RemoveCoinsCommand());
        this.getCommand("setcoins").setExecutor(new SetCoinsCommand());
        this.getCommand("pay").setExecutor(new PayCommand());
    }

    private void registerCoinUpdater() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> MySqlManager.updateCoins(), 20L*60*20, 20L*60*20);
    }

    public static CoinSystem getInstance() {
        return coinSystem;
    }

    public static CoinAPI getCoinAPI() {
        return coinAPI;
    }

    public static CacheManager getCacheManager() {
        return cacheManager;
    }

    public static MySQL getMySql() {
        return mySql;
    }
}
