package net.bausucht.runyx.coinsystem.coins;

import net.bausucht.runyx.coinsystem.CoinSystem;
import net.bausucht.runyx.coinsystem.coins.manager.CoinsChangeResponse;
import net.bausucht.runyx.coinsystem.events.PlayerCoinsChangeEvent;
import net.bausucht.runyx.coinsystem.events.PlayerPayPlayerEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class CoinAPI {

    private HashMap<UUID, Integer> coinCache = new HashMap<UUID, Integer>();

    public CoinAPI() {
        initialize();
    }

    // Get coins
    public int getCoins(Player player) {
        return getCoins(player.getUniqueId());
    }

    public int getCoins(UUID playerUuid) {
        return CoinSystem.getCacheManager().getCoins(playerUuid);
    }

    // Add coins
    public CoinsChangeResponse addCoins(Player player, int amount) {
        return setCoins(player, addCoins(getCoins(player), amount));
    }

    public CoinsChangeResponse addCoins(UUID playerUuid, int amount) {
        return setCoins(playerUuid, addCoins(getCoins(playerUuid), amount));
    }

    private int addCoins(int currentAmount, int addAmount) {
        return currentAmount + addAmount;
    }

    // Remove coins
    public CoinsChangeResponse removeCoins(Player player, int amount) {
        return setCoins(player, removeCoins(getCoins(player), amount));
    }

    public CoinsChangeResponse removeCoins(UUID playerUuid, int amount) {
        return setCoins(playerUuid, removeCoins(getCoins(playerUuid), amount));
    }

    private int removeCoins(int currentAmount, int removeAmount) {
        int rem = currentAmount - removeAmount;
        return (rem < 1) ? 0 : rem;
    }

    // Pay coins
    public CoinsChangeResponse payCoins(Player sender, Player receiver, int amount) {
        return payCoinsFunc(sender, receiver, amount);
    }

    public CoinsChangeResponse payCoins(Player sender, UUID receiver, int amount) {
        return payCoinsFunc(sender, receiver, amount);
    }

    private CoinsChangeResponse payCoinsFunc(Player sender, Object receiver, int amount) {
        CoinsChangeResponse coinsChanged;
        if(removeCoins(getCoins(sender), amount) >= 0)
        {
            coinsChanged = setCoins(sender, removeCoins(getCoins(sender), amount));
            if(coinsChanged.equals(CoinsChangeResponse.SUCCESS)) {
                if(receiver instanceof Player) {
                    coinsChanged = setCoins((Player) receiver, addCoins(getCoins((Player) receiver), amount));
                    PlayerPayPlayerEvent playerPayPlayerEvent = new PlayerPayPlayerEvent(sender, (Player) receiver, amount);
                    Bukkit.getPluginManager().callEvent(playerPayPlayerEvent);
                }
                else
                {
                    coinsChanged = setCoins((UUID) receiver, removeCoins(getCoins((UUID) receiver), amount));
                }
            }
        }
        else
        {
            coinsChanged = CoinsChangeResponse.NOT_ENOUGH_MONEY;
        }

        return coinsChanged;
    }

    // Set coins
    public CoinsChangeResponse setCoins(Player player, int amount) {
        PlayerCoinsChangeEvent playerCoinsChangeEvent = new PlayerCoinsChangeEvent(player, getCoins(player), amount);
        Bukkit.getServer().getPluginManager().callEvent(playerCoinsChangeEvent);

        return setCoins(player.getUniqueId(), amount);
    }

    public CoinsChangeResponse setCoins(UUID playerUuid, int amount) {
        return CoinSystem.getCacheManager().updateCoins(playerUuid, amount);
    }

    private void initialize() {

    }
}
