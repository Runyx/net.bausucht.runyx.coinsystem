package net.bausucht.runyx.coinsystem.coins.manager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CacheManager {

    private static final Map<UUID, Integer> playersData = Collections.synchronizedMap(new HashMap<>());

    public int getCoins(UUID uuid) {
        if(playersData.containsKey(uuid)) {
            return playersData.get(uuid);
        }
        else
        {
            MySqlManager.queryCoins(uuid);

            return -1;
        }
    }

    public CoinsChangeResponse updateCoins(UUID uuid, int coins) {
        if(coins >= 0) {
            playersData.put(uuid, coins);
            return CoinsChangeResponse.SUCCESS;
        }
        else
        {
            playersData.put(uuid, 0);
            return CoinsChangeResponse.NOT_ENOUGH_MONEY;
        }
    }

    public void removeCoinsFromCache(UUID uuid) {
        if(playersData.containsKey(uuid)) {
            playersData.remove(uuid);
        }
    }

    public Map<UUID, Integer> getPlayersData() {
        return playersData;
    }
}
