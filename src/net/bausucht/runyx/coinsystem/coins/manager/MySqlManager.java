package net.bausucht.runyx.coinsystem.coins.manager;

import net.bausucht.runyx.coinsystem.CoinSystem;
import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MySqlManager {

    public static void queryCoins(UUID uuid) {

        AsyncMySQL.queryCoinsAsync(uuid, new AsyncMySQL.Callback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                CoinSystem.getCacheManager().getPlayersData().put(uuid, result.intValue());
            }

            @Override
            public void onFailure(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    private static void createUser(UUID uuid) {
        AsyncMySQL.createUserAsync(uuid, new AsyncMySQL.Callback<Integer>() {
            @Override
            public void onSuccess(Integer result) {

            }

            @Override
            public void onFailure(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    public static boolean updateCoins() {
        Map<UUID, Integer> playerCoins = CoinSystem.getCacheManager().getPlayersData();
        if(playerCoins.size() > 0)
        {
            Object[] keySet = playerCoins.keySet().toArray();
            for(int i = 0; i < keySet.length; i++) {
                UUID playerUUID = (UUID) keySet[i];
                updateCoins(playerUUID);
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    public static boolean updateCoins(UUID uuid) {
        Map<UUID, Integer> playerCoins = CoinSystem.getCacheManager().getPlayersData();
        if( (uuid != null) && (playerCoins.containsKey(uuid)) ) {
            AsyncMySQL.updateCoinsAsync(uuid, playerCoins.get(uuid), new AsyncMySQL.Callback<Integer>() {
                @Override
                public void onSuccess(Integer result) {
                    Bukkit.broadcastMessage("UpdateCoins: " + result.intValue());
                }

                @Override
                public void onFailure(Throwable error) {
                    error.printStackTrace();
                }
            });
        }
        else
        {
            return false;
        }
        return true;
    }
}
