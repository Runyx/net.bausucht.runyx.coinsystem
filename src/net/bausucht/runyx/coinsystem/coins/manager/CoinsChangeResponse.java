package net.bausucht.runyx.coinsystem.coins.manager;

public enum CoinsChangeResponse {
    SUCCESS,
    NOT_ENOUGH_MONEY,
    ERROR
}
