package net.bausucht.runyx.coinsystem.coins.manager;

import net.bausucht.runyx.coinsystem.CoinSystem;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class AsyncMySQL {

    public static void queryCoinsAsync(final UUID uuid, final Callback<Integer> callback) {
        Bukkit.getScheduler().runTaskAsynchronously(CoinSystem.getInstance(), () -> {
            int result = 0;

            try {
                PreparedStatement preparedStatement = CoinSystem.getMySql().getConnection().prepareStatement("SELECT coins FROM coinsystem WHERE UUID = '" + uuid.toString() + "';");
                ResultSet resultSet = preparedStatement.executeQuery();
                if(resultSet.next()) {
                    int coins = resultSet.getInt("coins");
                    result = coins;
                    resultSet.close();
                }
            } catch (SQLException sqlException) {
                callback.onFailure(sqlException);
            }

            final int resultCallback = result;

            Bukkit.getScheduler().runTask(CoinSystem.getInstance(), () -> callback.onSuccess(resultCallback));
        });
    }

    public static void updateCoinsAsync(final UUID uuid, int coins, final Callback<Integer> callback) {
        Bukkit.getScheduler().runTaskAsynchronously(CoinSystem.getInstance(), () -> {
            int result = 0;

            try {
                PreparedStatement preparedStatement = CoinSystem.getMySql().getConnection().prepareStatement("UPDATE coinsystem SET coins = " + coins + " WHERE UUID = '" + uuid.toString() + "';");
                int updateSet = preparedStatement.executeUpdate();
                result = updateSet;
            } catch (SQLException sqlException) {
                callback.onFailure(sqlException);
            }

            final int resultCallback = result;

            Bukkit.getScheduler().runTask(CoinSystem.getInstance(), () -> callback.onSuccess(resultCallback));
        });
    }

    public static void createUserAsync(final UUID uuid, final Callback<Integer> callback) {
        Bukkit.getScheduler().runTaskAsynchronously(CoinSystem.getInstance(), () -> {
            final int result = 0;

            try {
                PreparedStatement preparedStatement = CoinSystem.getMySql().getConnection().prepareStatement("INSERT INTO coinsystem (UUID, coins) VALUES('" + uuid.toString() + "', 0);");
                preparedStatement.execute();
            } catch (SQLException sqlException) {
                callback.onFailure(sqlException);
            }

            Bukkit.getScheduler().runTask(CoinSystem.getInstance(), () -> callback.onSuccess(result));
        });
    }

    public interface Callback<T> {
        void onSuccess(T result);
        void onFailure(Throwable error);
    }
}
