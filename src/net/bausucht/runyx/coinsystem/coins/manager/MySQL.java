package net.bausucht.runyx.coinsystem.coins.manager;

import net.bausucht.runyx.coinsystem.CoinSystem;

import java.sql.*;

public class MySQL {

    // Config values
    private String host;
    private int port;
    private String database;
    private String username;
    private String password;

    private Connection connection;

    public MySQL(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        openConnection();
    }

    private void openConnection() {
        try {
            try {
                if ((connection != null) && (!connection.isClosed())) {
                    return;
                }

                synchronized (CoinSystem.getInstance()) {
                    if ((connection != null) && (!connection.isClosed())) {
                        return;
                    }

                    Class.forName("com.mysql.jdbc.Driver");
                    connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.username, this.password);
                }
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeResources(ResultSet resultSet, PreparedStatement preparedStatement) {
        if(resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
            }
        }
        if(preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
            }
        }
    }
}
