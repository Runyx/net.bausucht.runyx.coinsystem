package net.bausucht.runyx.coinsystem.listeners;

import net.bausucht.runyx.coinsystem.CoinSystem;
import net.bausucht.runyx.coinsystem.coins.manager.MySqlManager;
import net.bausucht.runyx.coinsystem.events.PlayerCoinsChangeEvent;
import net.bausucht.runyx.coinsystem.events.PlayerPayPlayerEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class SpecialCoinListeners implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        CoinSystem.getCacheManager().removeCoinsFromCache(player.getUniqueId());
        MySqlManager.queryCoins(player.getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if(MySqlManager.updateCoins(player.getUniqueId())) {
            CoinSystem.getCacheManager().removeCoinsFromCache(player.getUniqueId());
        }
    }
}
